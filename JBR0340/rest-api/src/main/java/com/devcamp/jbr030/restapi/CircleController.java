package com.devcamp.jbr030.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleController {
    @CrossOrigin
    @GetMapping("/circleRest")
    public static String main(String[] args) {
        Circle circle1 = new Circle(2.0);
        Circle circle2 = new Circle(3.0);

        return String.format("Dien tich hinh tron thu 1 = %s, Dien tich hinh tron thu 2 = %s", circle1.getArea(),circle2.getArea());
         
    }
}
