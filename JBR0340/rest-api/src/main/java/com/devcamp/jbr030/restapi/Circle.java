package com.devcamp.jbr030.restapi;

public class Circle {
    double radius = 1.0;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        double getArea = Math.pow( radius, 2) * Math.PI;
        return getArea;
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }

    
}
