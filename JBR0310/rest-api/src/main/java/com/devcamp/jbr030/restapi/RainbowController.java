package com.devcamp.jbr030.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RainbowController {
    @CrossOrigin
    @GetMapping("/rainbow")
    public static ArrayList<String> main(String args[]){  
        ArrayList<String> listRainbow =new ArrayList<String>();//Creating arraylist    
        listRainbow.add("red");//Adding object in arraylist    
        listRainbow.add("orange");    
        listRainbow.add("yellow");    
        listRainbow.add("green"); 
        listRainbow.add("blue");
        listRainbow.add("indigo"); 
        listRainbow.add("'violet");    
            //Printing the arraylist object   
            return listRainbow;  
       }  
}
