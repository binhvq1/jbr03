package com.devcamp.jbr300.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
    @CrossOrigin
    @GetMapping("/accounts")
    public static ArrayList<Account> main(String[] args) {
        Account account1 = new Account("001", "Hung", 10000);
        Account account2 = new Account("002", "Hoa", 20000);
        Account account3 = new Account("003", "Sung", 30000);

        System.out.println(account1); //in toString
        System.out.println(account2);
        System.out.println(account3);

        ArrayList<Account> listAccounts = new ArrayList<>();
        listAccounts.add(account1);
        listAccounts.add(account2);
        listAccounts.add(account3);

        return listAccounts;
    }
}
