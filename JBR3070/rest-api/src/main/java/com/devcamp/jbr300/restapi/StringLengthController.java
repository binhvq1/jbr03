package com.devcamp.jbr300.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StringLengthController {
    @CrossOrigin
    @GetMapping("/length")
    public static String main(String[] args) {
        String request = "Java basic first promgram";
        return String.format("Chiều dài của chuỗi = %s", request.length());
    }
}
