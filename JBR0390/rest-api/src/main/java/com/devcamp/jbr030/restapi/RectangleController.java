package com.devcamp.jbr030.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RectangleController {
    @CrossOrigin
    @GetMapping("/rectangle-area")
    public String getArea(@RequestParam("length") float lenght, @RequestParam("width") float width) {
        Rectangle rectangle = new Rectangle(lenght,width);
        return String.format("Diện tích hình chữ nhật = %s", rectangle.getArea());
    }

    @GetMapping("/rectangle-perimeter")
    public String getPerimeter(@RequestParam("length") float lenght, @RequestParam("width") float width) {
        Rectangle rectangle = new Rectangle(lenght,width);
        return String.format("Chu Vi chữ nhật = %s", rectangle.getPerimeter());
    }
}
