package com.devcamp.jbr030.restapi;

public class Rectangle {
    float lenght = (float) 1.0;
    float width = (float) 1.0;

    
    public Rectangle() {
    }

    public Rectangle(float lenght, float width) {
        this.lenght = lenght;
        this.width = width;
    }

    public float getLenght() {
        return lenght;
    }

    public void setLenght(float lenght) {
        this.lenght = lenght;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public double getArea() {
        double getArea = lenght * width;
        return getArea;
    }

    public double getPerimeter() {
        double getPerimeter = (lenght + width) * 2;
        return getPerimeter;

    }

    @Override
    public String toString() {
        return "Rectangle [lenght=" + lenght + ", width=" + width + "]";
    }

    
}
