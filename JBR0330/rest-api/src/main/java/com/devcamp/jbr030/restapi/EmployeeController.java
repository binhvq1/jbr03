package com.devcamp.jbr030.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    @CrossOrigin
    @GetMapping("/employee")
    public static ArrayList<Employee> main(String[] args) {
        Employee employee1 = new Employee(01, "Hung", "Vo", 10000);
        Employee employee2 = new Employee(02, "Nam", "Ngo", 20000);
        Employee employee3 = new Employee(03, "Phuong", "Mac", 30000);

        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);

        return employees;
    }
}
