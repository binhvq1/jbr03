package com.devcamp.jbr030.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SplitStringController {
    @CrossOrigin
    @GetMapping("/splitString")
    public static ArrayList<String> main(String args[]) {
        ArrayList<String> listSplitString = new ArrayList<>();
        String s1 = "java string split method by viettuts";
        String[] words = s1.split("\\s");//tach chuoi dua tren khoang trang
        //su dung vong lap foreach de in cac element cua mang chuoi thu duoc
        for (String w : words) {
            listSplitString.add(w);
        }

        return listSplitString;
    }
}
