package com.devcamp.jbr030.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvoiceItemController {
    @CrossOrigin
    @GetMapping("/invoices")
    public static ArrayList<InvoiceItem> main(String[] args) {
        InvoiceItem invoiceItem1 = new InvoiceItem("A001", "Bot Mi", 10, 10000);
        InvoiceItem invoiceItem2 = new InvoiceItem("B002", "Bot Gao", 20, 15000);
        InvoiceItem invoiceItem3 = new InvoiceItem("C003", "Bot San", 30, 20000);
        
        ArrayList<InvoiceItem> listInvoiceItems = new ArrayList<>();
        listInvoiceItems.add(invoiceItem1);
        listInvoiceItems.add(invoiceItem2);
        listInvoiceItems.add(invoiceItem3);

        return listInvoiceItems;
    }
}
