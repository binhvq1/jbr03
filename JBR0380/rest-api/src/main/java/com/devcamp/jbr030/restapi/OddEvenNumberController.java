package com.devcamp.jbr030.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OddEvenNumberController {
    @CrossOrigin
    @GetMapping("/checknumber")
    
    public String getRequest(@RequestParam("request") int request) {
        String valueString;
        if(request%2==0)
        {
            valueString = "Đây là số chẵn!";
        }
        else
        {
            valueString = "Đây là số lẻ!";
        }
        return valueString;
    }
}
